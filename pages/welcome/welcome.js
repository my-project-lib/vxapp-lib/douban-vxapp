Page({
  goToPostPage: function (event) {
        wx.switchTab({
            url: "../post/post",
            success: function () {
                console.log("jump success")
            },
            fail: function () {
                console.log("jump failed")
            },
            complete: function () {
                console.log("jump complete")
            }
        });
    },
    onUnload: function (event) {
        console.log("page is unload")
    },
    onHide: function (event) {
        console.log("page is hide")
    },
     // 用户登录授权
  login() {
    console.log('用户点击登录授权');
    // 获取用户信息
    wx.getUserProfile({
      desc: '用于完善会员资料',
      success: res => {
        let userInfo = res.userInfo;
        // 登录授权成功，保存用户信息到缓存
        wx.setStorageSync('userInfo', userInfo)
 
        this.setData({
          userInfo: userInfo
        });
      }
    })
  },
})